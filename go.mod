module gitlab.com/perinet/periMICA-container/service/eolperisnoop

go 1.22

toolchain go1.22.4

require (
	github.com/eclipse/paho.mqtt.golang v1.4.3
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20240624145344-b06586abad1b
	gitlab.com/perinet/generic/apiservice/mqttclient v1.0.0-rc.2.0.20240625093523-a85a7227dd8c
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20240621123748-68fb070dfc7d
	gitlab.com/perinet/generic/lib/httpserver v1.0.1-0.20240621122345-b2675d019c46
	gitlab.com/perinet/generic/lib/utils v1.0.1-0.20240621122644-d12809863382
	gitlab.com/perinet/periMICA-container/apiservice/eolperisnoop v0.0.0-20240625095438-f625d88b08b4
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20240624145151-3c38f532f641
	gitlab.com/perinet/periMICA-container/apiservice/network v0.0.0-20240624145207-d9c6091ae9da
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.3-0.20240624145325-1bd003316553
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20240624082929-5c8fee3c2975
	gitlab.com/perinet/periMICA-container/apiservice/ssh v0.0.0-20240624145255-5cbf6374e8f8
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.2-0.20240210093433-b8dc0fc11e7e // indirect
	github.com/tidwall/gjson v1.17.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	golang.org/x/exp v0.0.0-20240613232115-7f521ea00fb8 // indirect
	golang.org/x/net v0.26.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
)
