/*
 * Copyright (c) 2018-2023 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package main

import (
	"encoding/json"
	"log"
	"strings"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/apiservice/mqttclient"
	"gitlab.com/perinet/generic/apiservice/staticfiles"
	httpserver "gitlab.com/perinet/generic/lib/httpserver"
	auth "gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/eolperisnoop"
	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	security "gitlab.com/perinet/periMICA-container/apiservice/security"
	"gitlab.com/perinet/periMICA-container/apiservice/ssh"
)
 
 var (
	 logger         log.Logger = *log.Default()
	 securityConfig security.SecurityConfig
	 subscriber_endpoint mqttclient.MQTTClientSubscriberEndpoint
 )

 const (
	START_COMMAND = "start PRN100069"
	GOLDEN_TESTS_COMMAND = "goldentests PRN100069"
	RETRY_COMMAND = "retry PRN100069"
	LOG_PATH_OUTPUT = "/var/lib/api_service/log_output.txt"
 )

 func Resubscribe() {
	data := intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err := json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}
	application_name := nodeInfo.Config.ApplicationName

	topic := application_name + "/scanner" // subscriber to the application scanner
	subscriber_endpoint.Cancel()
	subscriber_endpoint = mqttclient.NewSubscriberEndpoint(topic, mqtt_msg_handler)
	log.Println("Resubscribe -> subscribed to topic: " + topic)
}

var mqtt_msg_handler mqtt.MessageHandler = func(_ mqtt.Client, msg mqtt.Message) {
	
	log.Println("message received:", string(msg.Payload()))
	command := string(msg.Payload()[:])
	command = strings.TrimSpace(command)

	data := intHttp.Get(eolperisnoop.Eolperisnoop_Info_Get, nil)
	var eol_info eolperisnoop.Eolperisnoop_info
	err := json.Unmarshal(data, &eol_info)
	if err != nil {
		log.Println("Failed to fetch eol_info: ", err.Error())
	}
	
	if (eol_info.Status != eolperisnoop.Status.RUNNING) {

		switch command {
		case START_COMMAND:
			log.Println("start tests from scanner command!")
			intHttp.Patch(eolperisnoop.Eolperisnoop_Execute_Set, []byte{}, nil)

		case GOLDEN_TESTS_COMMAND:
			log.Println("init golden tests from scanner command!")
			intHttp.Patch(eolperisnoop.Eolperisnoop_Execute_Golden_Tests_Set, []byte{}, nil)

		case RETRY_COMMAND:
			log.Println("retry received from scanner command!")
			intHttp.Patch(eolperisnoop.Eolperisnoop_Retry_Set, []byte{}, nil)

		default:
			log.Println("Error: Unrecognized command read from Scanner ")
		}
	}
}
 
 func init() {
	log.SetPrefix("eolperisnoop service: ")
 
	var data []byte
	services := []string{"node", "security", "lifecycle", "dns-sd", "ssh", "eolperisnoop"}
	data, _ = json.Marshal(services)
	intHttp.Put(node.ServicesSet, data, nil)

	var err error
	err = json.Unmarshal(intHttp.Get(security.Security_Config_Get, nil), &securityConfig)
	if err != nil {
		log.Println("Failed to fetch SecurityConfig: ", err.Error())
	}

	// set security authentication method
	auth.SetAuthMethod(securityConfig.ClientAuthMethod)
 
	data = intHttp.Get(node.NodeInfoGet, nil)
	var nodeInfo node.NodeInfo
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		log.Println("Failed to fetch NodeInfo: ", err.Error())
	}
 
	 dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443, Ttl: 30}
	 dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+string(nodeInfo.ApiVersion))
	 dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	 dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	 dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "error_status="+nodeInfo.ErrorStatus.String())

	 data, _ = json.Marshal(dnssdServiceInfo)
	 intHttp.Patch(dnssd.DNSSDServiceInfoAdvertiseSet, data, map[string]string{"service_name": dnssdServiceInfo.ServiceName})

	 Resubscribe()
 }
 
 func main() {
	logger.Println("starting")
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(ssh.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())
	httpserver.AddPaths(eolperisnoop.PathsGet())
 
 
	httpserver.SetCertificates(httpserver.Certificates{
		 CaCert:      intHttp.Get(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		 HostCert:    intHttp.Get(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		 HostCertKey: intHttp.Get(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	 })
 
	httpserver.ListenAndServe("[::]:443")
 }
 
